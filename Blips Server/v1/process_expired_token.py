__author__ = 'andrewapperley'

import database
from sqlalchemy import exc
from user import ActiveUser
from notification import RegisteredNotificationUserModel, NotificationModel
import sys
from datetime import datetime
import calendar
import logging


def process():

    if len(sys.argv) > 1:
        a = sys.argv[1]
        database.createDatabase(a)
    else:
        database.createDatabase()

    session = database.DBSession()

    active_users = session.query(ActiveUser).all()

    if len(active_users) > 0:
        time_stamp_now = calendar.timegm(datetime.utcnow().timetuple())
        for user in active_users:
            toke_time = calendar.timegm(user.expiry_date.utctimetuple())
            if toke_time < time_stamp_now:
                session.delete(user)
                session.query(RegisteredNotificationUserModel).filter(RegisteredNotificationUserModel.user_id == user.user_id).update({'registered_user_token': ""}, synchronize_session='fetch')
                notifications = session.query(NotificationModel).filter(NotificationModel.notification_receiver_id == user.user_id).all()
                if len(notifications) > 0:
                    for notification in notifications:
                        session.delete(notification)
        try:
            session.commit()
            session.close()
        except exc.SQLAlchemyError:
                logging.exception('')


if len(sys.argv) > 1:
    process()